﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Enums
{
    public static class AnimationLabelConstants
    {
        public const string IdleLabel = "Idle";
        public const string WalkingBottomLabel = "WalkingBottom";
        public const string WalkingLeftLabel = "WalkingLeft";
        public const string WalkingRightLabel = "WalkingRight";
        public const string WalkingTopLabel = "WalkingTop";
    }
}